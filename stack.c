/**
 * \file stack.c
 * \brief generic stack library allowing to manage stacks generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-03-25 Wed 10:37 PM
 * \copyright
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/
#include "stack.h"

Stack * Stack_malloc(void)
{
	Stack * stack;
	stack = malloc(sizeof(Stack));
	if (stack == NULL) {
		fprintf(stderr, "error: %s.", strerror(errno));
		exit(EXIT_FAILURE);
	}
	stack->head = NULL;
	stack->size = 0;
	return stack;
}

bool Stack_isempty(const Stack * const stack)
{
	return stack->size == 0;
}

StackElement * Stack_at_top(const Stack * const stack)
{
	return stack->head;
}

void Stack_swap(Stack * const stack0, Stack * const stack1)
{
	Stack tmp;
	tmp.head = stack0->head;
	tmp.size = stack0->size;
	stack0->head = stack1->head;
	stack0->size = stack1->size;
	stack1->head = tmp.head;
	stack1->size = tmp.size;
}

static StackElement * StackElement_mallocinit(const void * const value, const size_t offset)
{
	StackElement * element;
	element = malloc(sizeof(StackElement));
	if (element == NULL) {
		fprintf(stderr, "error: %s.", strerror(errno));
		exit(EXIT_FAILURE);
	}
	if (offset == 0) {
		fprintf(stderr, "error: can't allocate cause offset is equal to 0.\n");
		exit(EXIT_FAILURE);
	}
	element->value = malloc(offset);
	if (element->value == NULL) {
		fprintf(stderr, "error: %s.", strerror(errno));
		exit(EXIT_FAILURE);
	}
	memcpy(element->value, value, offset);
	return element;
}

void Stack_push(Stack * const stack, const void * const value, const size_t offset)
{
	StackElement * element;
	element = StackElement_mallocinit(value, offset);
	element->next = stack->head;
	stack->head = element;
	stack->size++;
}

void Stack_pop(Stack * const stack)
{
	StackElement * tmp;
	if (Stack_isempty(stack)) {
		fprintf(stderr, "error: cannot pop en empty stack.\n");
		exit(EXIT_FAILURE);
	}
	free(stack->head->value);
	tmp = stack->head;
	stack->head = tmp->next;
	free(tmp);
	stack->size--;
}

void Stack_free(Stack * * const stack)
{
	while (! Stack_isempty(*stack)) {
		Stack_pop(*stack);
	}
	free(*stack);
	*stack = NULL;
}
