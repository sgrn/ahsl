/**
 * \file stack.h
 * \brief generic stack library allowing to manage stacks generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-03-25 Wed 10:37 PM
 * \copyright
 * Copyright 2020 Sangsoic library author
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *             http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __STACK_H__
	#define __STACK_H__

	#include <stdio.h>
	#include <stdlib.h>
	#include <errno.h>
	#include <string.h>
	#include <stdbool.h>

	/**
	 * \def STACK
	 * \brief User-level working objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK(TYPE, NAME) \
		typedef struct StackElement##NAME { \
			TYPE * value; \
			struct StackElement##NAME * next; \
		} StackElement##NAME; \
		typedef struct { \
			StackElement##NAME * head; \
			size_t size; \
		} Stack##NAME;

	/**
	 * \def STACK_MALLOC
	 * \brief Wrapper routine for \a Stack_malloc routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_MALLOC(TYPE, NAME) \
		static Stack##NAME * Stack##NAME##_malloc(void) { \
			return (Stack##NAME *)Stack_malloc(); \
		}

	/**
	 * \def STACK_ISEMPTY
	 * \brief Wrapper routine for \a Stack_isempty routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_ISEMPTY(TYPE, NAME) \
		static bool Stack##NAME##_isempty(const Stack##NAME * const stack) \
		{ \
			return Stack_isempty((Stack *)stack); \
		}

	/**
	 * \def STACK_AT_TOP
	 * \brief Wrapper routine for \a Stack_at_top routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_AT_TOP(TYPE, NAME) \
		static StackElement##NAME * Stack##NAME##_at_top(const Stack##NAME * const stack) \
		{ \
			return (StackElement##NAME *)Stack_at_top((Stack *)stack); \
		}

	/**
	 * \def STACK_SET_TOP
	 * \brief Wrapper routine that sets the data of the top element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_SET_TOP(TYPE, NAME) \
		static void Stack##NAME##_set_top(const Stack##NAME * const stack, const TYPE value) \
		{ \
			*((TYPE *)(Stack_at_top((Stack *)stack)->value)) = value; \
		}

	/**
	 * \def STACK_GET_TOP
	 * \brief Wrapper routine that returns the value of the top element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_GET_TOP(TYPE, NAME) \
		static TYPE Stack##NAME##_get_top(const Stack##NAME * const stack) \
		{ \
			return *((TYPE *)(Stack_at_top((Stack *)stack)->value)); \
		}

	/**
	 * \def STACK_SWAP
	 * \brief Wrapper routine for \a Stack_swap routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_SWAP(TYPE, NAME) \
		static void Stack##NAME##_swap(Stack##NAME * const stack0, Stack##NAME * const stack1) \
		{ \
			Stack_swap((Stack *)stack0, (Stack *)stack1); \
		}

	/**
	 * \def STACK_PUSH
	 * \brief Wrapper routine for \a Stack_push routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_PUSH(TYPE, NAME) \
		static void Stack##NAME##_push(Stack##NAME * const stack, const TYPE value) \
		{ \
			Stack_push((Stack *)stack, &value, sizeof(TYPE)); \
		}

	/**
	 * \def STACK_POP
	 * \brief Wrapper routine for \a Stack_pop routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_POP(TYPE, NAME) \
		static void Stack##NAME##_pop(Stack##NAME * const stack) \
		{ \
			Stack_pop((Stack *)stack); \
		}

	/**
	 * \def STACK_FREE
	 * \brief Wrapper routine for \a Stack_free routine.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \date 2020-03-26 Thu 12:08 AM
	 * \param TYPE C datatype
	 * \param NAME Name to give to generic object
	 */
	#define STACK_FREE(TYPE, NAME) \
		static void Stack##NAME##_free(Stack##NAME * * const stack) \
		{ \
			Stack_free((Stack * *)stack); \
		}

	/**
	* \struct StackElement
	* \brief Represents a Stack element object type.
	* \author Sangsoic <sangsoic@protonmail.com>
	* \version 0.1
	* \date 2020-03-25 Wed 11:55 PM
	* \var void * value
	* Address of the generic data.
	* \var StackElement * next
	* Address of the next stack element.
	*/
	typedef struct StackElement {
		void * value;
		struct StackElement * next;
	} StackElement;

	/**
	* \struct Stack
	* \brief Represents a Stack object type.
	* \author Sangsoic <sangsoic@protonmail.com>
	* \version 0.1
	* \date 2020-03-25 Wed 11:56 PM 
	* \var StackElement * head
	* Address of the top element.
	* \var size_t size
	* Number of element contained within the stack.
	*/
	typedef struct {
		StackElement * head;
		size_t size;
	} Stack;

	/**
	 * \fn Stack * Stack_malloc(void)
	 * \brief Allocates stack objects.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:45 PM
	 * \return Address of the newly allocated Stack object;
	 */
	Stack * Stack_malloc(void);

	/**
	 * \fn bool Stack_isempty(const Stack * const stack)
	 * \brief Tests if a given stack is empty.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:50 PM
	 * \param stack A given stack.
	 * \return true if stack is empty else false.
	 */
	bool Stack_isempty(const Stack * const stack);

	/**
	 * \fn StackElement * Stack_at_top(const Stack * const stack)
	 * \brief Returns the address of the top element.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:52 PM
	 * \param stack A given stack.
	 * \return Address of the top element.
	 */
	StackElement * Stack_at_top(const Stack * const stack);

	/**
	 * \fn void Stack_swap(Stack * const stack0, Stack * const stack1)
	 * \brief Swaps the content of two given stacks.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:55 PM
	 * \param stack0 A given stack.
	 * \param stack1 A given stack.
	 */
	void Stack_swap(Stack * const stack0, Stack * const stack1);

	/**
	 * \fn void Stack_push(Stack * const stack, const void * const value, const size_t offset)
	 * \brief Adds a given element to the top of the stack.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 10:57 PM
	 * \param stack A given stack.
	 * \param value The address of the value to add in the stack.
	 * \param offset Size in byte of the given value.
	 */
	void Stack_push(Stack * const stack, const void * const value, const size_t offset);
	
	/**
	 * \fn void Stack_pop(Stack * const stack)
	 * \brief Removes the top element of the stack.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 11:01 PM
	 * \param stack A given stack.
	 */
	void Stack_pop(Stack * const stack);

	/**
	 * \fn void Stack_free(Stack * * const stack)
	 * \brief Frees memory taken by a given stack.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 2020-03-25 Wed 11:03 PM
	 * \param stack Address of the pointer containing the stack to be freed.
	 */
	void Stack_free(Stack * * const stack);

#endif
