var searchData=
[
  ['stack_5fat_5ftop',['Stack_at_top',['../d3/dbb/stack_8c.html#aff7aa672c12fa8d018670bbb8714e3bf',1,'Stack_at_top(const Stack *const stack):&#160;stack.c'],['../d7/de0/stack_8h.html#ad7a4a77e5d4604ed0efcb21633349846',1,'Stack_at_top(const Stack *const stack):&#160;stack.c']]],
  ['stack_5ffree',['Stack_free',['../d3/dbb/stack_8c.html#a91f4f405d84c87ce08d786d08055e0a3',1,'Stack_free(Stack **const stack):&#160;stack.c'],['../d7/de0/stack_8h.html#a91f4f405d84c87ce08d786d08055e0a3',1,'Stack_free(Stack **const stack):&#160;stack.c']]],
  ['stack_5fisempty',['Stack_isempty',['../d3/dbb/stack_8c.html#a8484a124927ac61bcbb67e83a0afd031',1,'Stack_isempty(const Stack *const stack):&#160;stack.c'],['../d7/de0/stack_8h.html#a8484a124927ac61bcbb67e83a0afd031',1,'Stack_isempty(const Stack *const stack):&#160;stack.c']]],
  ['stack_5fmalloc',['Stack_malloc',['../d3/dbb/stack_8c.html#a18e02fdd3e1567207962469e3ba32dfa',1,'Stack_malloc(void):&#160;stack.c'],['../d7/de0/stack_8h.html#a408ae28da1ef1693fd82db19ed8002ca',1,'Stack_malloc(void):&#160;stack.c']]],
  ['stack_5fpop',['Stack_pop',['../d3/dbb/stack_8c.html#a2c4c25365a3c92c66badc33a5c24d0b1',1,'Stack_pop(Stack *const stack):&#160;stack.c'],['../d7/de0/stack_8h.html#a2c4c25365a3c92c66badc33a5c24d0b1',1,'Stack_pop(Stack *const stack):&#160;stack.c']]],
  ['stack_5fpush',['Stack_push',['../d3/dbb/stack_8c.html#a304f92ceaf3b97cc546060c34e18290d',1,'Stack_push(Stack *const stack, const void *const value, const size_t offset):&#160;stack.c'],['../d7/de0/stack_8h.html#a304f92ceaf3b97cc546060c34e18290d',1,'Stack_push(Stack *const stack, const void *const value, const size_t offset):&#160;stack.c']]],
  ['stack_5fswap',['Stack_swap',['../d3/dbb/stack_8c.html#a29567213d52233b8a20883e1c2f9787d',1,'Stack_swap(Stack *const stack0, Stack *const stack1):&#160;stack.c'],['../d7/de0/stack_8h.html#a29567213d52233b8a20883e1c2f9787d',1,'Stack_swap(Stack *const stack0, Stack *const stack1):&#160;stack.c']]]
];
