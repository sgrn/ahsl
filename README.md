# A handful generic stack C library

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

* This C stack library is [generic](https://en.wikipedia.org/wiki/Generic_programming).
* You can access the project documentation in the Doc/ directory.
* You can also read the next section of this page in order to understand basic usage.
* If you would like to know more about the general concept of stack [read this document](https://en.wikipedia.org/wiki/Stack_(abstract_data_type)).
* Feel free to submit changes.

## Usage

There is two way to access routines. The easiest is using wrapper routine with macros and the more advanced way is to call the original generic version of the routine. The following sections cover each techniques in depth.

Stack typed objects contains 2 fields:

* head which corresponds to the last element added to the stack
* size which is the current number of value contained inside the stack.

### Wrapper routine

This method allows you to call macros to define specially typed structures and routines.

#### Macro syntax

```
STACK_[ROUTINE IDENTIFIER](<TYPE>, <NAME>)
```

Where *[ROUTINE IDENTIFIER]* is the upper case routine name, if no routine name specified then it declares the specially typed stack structure, *\<TYPE\>* the stack type, *\<NAME\>* an identifier.  
To learn more about each macro read the documentation.

#### Example

```C
#include <stdio.h>
#include "stack.h"

STACK(int, Int)
STACK_MALLOC(int, Int)
STACK_ISEMPTY(int, Int)
STACK_SET_TOP(int, Int)
STACK_GET_TOP(int, Int)
STACK_PUSH(int, Int)
STACK_POP(int, Int)
STACK_FREE(int, Int)

int main (void)
{
	StackInt * stack;
	stack = StackInt_malloc();
	StackInt_push(stack, 5);
	StackInt_push(stack, 12);
	StackInt_push(stack, 90);
	printf("%d\n", StackInt_get_top(stack));
	StackInt_set_top(stack, 100);
	printf("%d\n", StackInt_get_top(stack));
	StackInt_pop(stack);
	printf("%d\n", StackInt_get_top(stack));
	StackInt_pop(stack);
	printf("%d\n", StackInt_get_top(stack));
	StackInt_pop(stack);
	if (StackInt_isempty(stack)) {
		printf("Stack is empty.\n");
	}
	stack = StackInt_free(stack);
	return 0;
}
```

### Generic routine

This method allows you to call generic routine directly, however you will need to handle casting manually.  
To learn more about each generic routine you can read the documentation.

#### Exemple

```C
#include <stdio.h>
#include "stack.h"

int main (void)
{
	Stack * stack;
	int tab[3] = {5, 12, 90};
	stack = Stack_malloc();
	Stack_push(stack, tab, sizeof(int));
	Stack_push(stack, tab + 1, sizeof(int));
	Stack_push(stack, tab + 2, sizeof(int ));
	printf("%d\n", *((int *)(Stack_at_top(stack)->value)));
	*((int *)stack->head->value) = 100;
	printf("%d\n", *((int *)(Stack_at_top(stack)->value)));
	Stack_pop(stack);
	printf("%d\n", *((int *)(Stack_at_top(stack)->value)));
	Stack_pop(stack);
	printf("%d\n", *((int *)(Stack_at_top(stack)->value)));
	Stack_pop(stack);
	if (Stack_isempty(stack)) {
		printf("Stack is empty.\n");
	}
	stack = Stack_free(stack);
	return 0;
}
```
